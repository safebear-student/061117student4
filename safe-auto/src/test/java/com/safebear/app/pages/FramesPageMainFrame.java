package com.safebear.app.pages;

import org.junit.Ignore;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * frames page main frame
 */
public class FramesPageMainFrame
{
    WebDriver driver;

    public FramesPageMainFrame(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    @Ignore
    public boolean checkCorrectPage()
    {
        return driver.getTitle().startsWith("");
    }
}
