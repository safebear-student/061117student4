package com.safebear.app.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Util class with getter methods for getDriver && getUrl
 */
public class Utils
{
    DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
    private WebDriver driver =  new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);

    //private WebDriver driver = new ChromeDriver();
    private String url = "http://automate.safebear.co.uk";

    public Utils() throws MalformedURLException
    {
    }

    public WebDriver getDriver()
    {
        return driver;
    }

    public String getUrl()
    {
        return url;
    }
}
